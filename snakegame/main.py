from loguru import logger

from snakegame.app.app import Application
from snakegame.app.settings import create_userspace_config
from snakegame.libs.logging import configure_logging


def run_app():
    configure_logging()
    logger.info(f"Application started!")
    create_userspace_config()

    app = Application()
    app.start()


if __name__ == "__main__":
    run_app()
