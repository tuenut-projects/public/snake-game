import importlib.resources as pkg_resources
import os
import shutil
from pathlib import Path
from warnings import warn

import yaml
from loguru import logger
from pydantic_settings import (
    BaseSettings,
    YamlConfigSettingsSource,
    SettingsConfigDict,
    PydanticBaseSettingsSource,
)

import snakegame
from snakegame.libs.decorators import as_singleton
from loguru import logger


class CliArguments(BaseSettings):
    model_config = SettingsConfigDict(
        cli_parse_args=True, env_prefix="tu_snakegame_"
    )
    dev: bool = False


class Settings(BaseSettings):
    model_config = SettingsConfigDict()

    fps: int = 30
    field_size: tuple[int, int] = (16, 16)
    screen_size: tuple[int, int] = (800, 600)

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return init_settings, YamlConfigSettingsSource(settings_cls)


@as_singleton
class SettingsManager:
    settings: Settings

    def __init__(self):
        self._config_path, self._read_only = get_config_path()
        logger.debug(f"Use config file at {self._config_path}")

        Settings.model_config["yaml_file"] = self._config_path

        self.load()

    def load(self):
        self.settings = Settings()
        logger.debug(f"Settings loaded from {self._config_path}.")

    def save(self):
        if self._read_only:
            return

        settings = self.settings.model_dump(mode="json")
        with open(self._config_path, "w") as f:
            yaml.safe_dump(settings, f)

        logger.debug(f"Settings saved.")


def get_default_config_path() -> Path:
    return Path(pkg_resources.files(snakegame).joinpath("config.yaml"))


def get_userspace_config_path() -> Path:
    if os.name == "nt":
        path = Path(os.getenv("APPDATA")) / "snakegame" / "config.yaml"
    else:
        path = Path.home() / ".config" / "snakegame" / "config.yaml"

    return path


def get_config_path() -> tuple[Path, bool]:
    path = get_userspace_config_path()
    read_only = False
    if not path.exists():
        path = get_default_config_path()
        read_only = True

    return path, read_only


def create_userspace_config():
    if cli_arguments.dev:
        return

    userspace_config = get_userspace_config_path()
    if not userspace_config.exists():
        logger.debug(f"Creating user config at {userspace_config}")
        try:
            os.makedirs(userspace_config.parent, exist_ok=True)
            shutil.copy(get_default_config_path(), userspace_config)
        except:
            warn(
                "Can't create user config  for app, default will be used.",
                RuntimeWarning,
            )


cli_arguments = CliArguments()
