from pathlib import Path

import pygame
from loguru import logger
from pygame.event import Event

from snakegame.app.render.manager import RenderManager
from snakegame.app.settings import SettingsManager
from snakegame.app.state.manager import GameStateManager
from snakegame.libs.events_manager.manager import EventManager


class Application:
    def __init__(self):
        self.is_run: bool = False

        self.settings_manager = SettingsManager()

        pygame.init()
        self.clock = pygame.time.Clock()
        logger.debug("Pygame initialised.")

        self.events_manager = EventManager()
        self.render_manager = RenderManager()
        self.game_state_manager = GameStateManager()

        self.events_manager.subscribe(on_event=pygame.QUIT, callback=self.stop)
        self.events_manager.subscribe(on_key_down=pygame.K_q, callback=self.stop)

    def start(self):
        self.is_run = True
        self.__main_loop()

    def stop(self, event: Event = None):
        self.is_run = False

    def __main_loop(self):
        while self.is_run:
            self.events_manager.handle_events()
            self.game_state_manager.update()
            self.render_manager.update()
            self.clock.tick(self.settings_manager.settings.fps)
