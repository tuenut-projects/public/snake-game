import pygame


class RenderManager:
    def __init__(self):
        self.screen = pygame.display.set_mode((1024, 768))
        self.font = pygame.font.SysFont('mono', 12, bold=True)

    def update(self):
        pygame.display.flip()
