from __future__ import annotations

import enum

from snakegame.app.settings import SettingsManager


class Directions(enum.Enum):
    LEFT = (-1, 0)
    RIGHT = (1, 0)
    UP = (0, -1)
    DOWN = (0, 1)


class GameField:
    def __init__(self):
        self.__settings_manager = SettingsManager()

        self.x_size = self.__settings_manager.settings.field_size[0]
        self.y_size = self.__settings_manager.settings.field_size[1]
        self.map = []

        for x in range(self.x_size):
            row = []
            for y in range(self.y_size):
                cell = GameFieldCell(x, y, self)
                row.append(cell)
            self.map.append(row)

    def get_dto(self): ...

    def __str__(self):
        text = ""
        for row in self.map:
            text += " ".join([str(cell) for cell in row])
            text += "\n"

        return f"GameField({self.x_size}, {self.y_size}):\n{text}\n"

    __repr__ = __str__


class GameFieldCell:
    def __init__(self, x, y, field_map: GameField):
        self.x, self.y = x, y
        self.field_map = field_map

    def get_next(self, direction: Directions) -> GameFieldCell | None:
        next_x, next_y = self.x + direction.value[0], self.y + direction.value[1]
        if (
            0 <= next_x < self.field_map.x_size
            or 0 <= next_y < self.field_map.y_size
        ):
            return None
        else:
            return self.field_map.map[next_x][next_y]

    def __str__(self):
        return f"{self.x, self.y}"

    __repr__ = __str__
