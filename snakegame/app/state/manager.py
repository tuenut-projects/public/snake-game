from loguru import logger

from snakegame.app.state import GameField
from snakegame.libs.abc.state.manager import ABCGameStateManager


class GameStateManager(ABCGameStateManager):
    field: GameField

    def __init__(self):
        pass

    def create_new_game(self):
        self.field = GameField()
        logger.debug(f"New game field created: {self.field}")

    def update(self):
        pass
