import sys

from loguru import logger

WARN_ONLY_LOGGING = "WARN_ONLY_LOGGING"


def configure_logging():
    logger.remove()
    logger.add(sys.stderr, filter=warn_only_filter)


def warn_only_filter(record: dict):
    if record["extra"].get(WARN_ONLY_LOGGING):
        return record["level"].no == logger.level("WARNING").no
    return True
