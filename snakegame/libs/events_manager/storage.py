from collections import defaultdict

from .subscriptions import EventSubscription


class SubscriptionsStore:
    __store: dict[int, dict[str, EventSubscription]]
    __stored_indices: dict[str, int]  # {subscription.id: event.type}

    def __init__(self):
        self.__store = defaultdict(dict)
        self.__stored_indices = {}

    def add(self, subscription: EventSubscription) -> None:
        if subscription.id in self.__stored_indices:
            raise KeyError(f"Subscription <{subscription}> already exists.")

        self.__store[subscription.event_type][subscription.id] = subscription
        self.__stored_indices[subscription.id] = subscription.event_type

    def remove(self, subscription_id: str) -> bool:
        if subscription_id not in self.__stored_indices:
            return False

        event_type = self.__stored_indices[subscription_id]

        del self.__store[event_type][subscription_id]
        del self.__stored_indices[subscription_id]

        return True

    def __getitem__(self, event_type):
        return list(self.__store[event_type].values())
