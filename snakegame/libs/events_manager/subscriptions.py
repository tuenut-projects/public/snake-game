import enum
from typing import Callable, Any

from loguru import logger
from pygame.event import Event


class MouseButtons(enum.Enum):
    LMB = 1
    MMB = 2
    RMB = 3


class EventSubscription:
    callback: Callable[[Event], None]
    event_type: int
    subtype: int | None
    conditions: dict[str, Any]
    kwargs: list[str]
    as_args: bool

    def __init__(
        self,
        callback: Callable,
        event_type: int,
        subtype: int | None = None,
        conditions: dict[str, Any] | None = None,
    ):
        """
        :param event_type: Should be one of pygame events, like `pygame.KEYDOWN`
        :param callback: Callback method to handle event.
            Method interface may be generic(*args, **kwargs) - use param `kwargs`
             to say which attributes from Event object should be passed as their
             names **kwargs in callback, set `as_args=True` to pass it as *args.
        :param subtype: Used for user custom Event types. TODO: not fully implemented
        :param conditions: Use to check some Event attribute with value in this
            dict stored in key as attribute name.
        """

        logger.debug(
            f"Subscribe callback <{callback}> on event_type <{event_type}>."
        )
        logger.debug(f"Conditions <{conditions}>.")
        logger.debug(f"Subtype <{subtype}>")

        index = id(callback)

        self.callback = callback
        self.event_type = event_type

        self.subtype = subtype
        self.conditions = conditions if conditions else {}

        self.__id = f"{str(event_type)}.{index}"

    @property
    def id(self):
        return self.__id

    def check_subtype(self, event):
        try:
            return event.subtype == self.subtype
        except AttributeError:
            logger.debug(f"Event <{event}> has no subtype of <{self.subtype}>")
            return False

    def check_conditions(self, event):
        if not self.conditions:
            return True

        try:
            return all(
                [
                    self.__check_condition(getattr(event, attr), value)
                    for attr, value in self.conditions.items()
                ]
            )
        except AttributeError:
            logger.debug(
                f"Check event <{event}> conditions for <{self.conditions}> not"
                " successful."
            )
            return False

    @staticmethod
    def __check_condition(event_value, expected_value):
        if isinstance(expected_value, (list, tuple)):
            return event_value in expected_value
        else:
            return event_value == expected_value

    def __repr__(self):
        return (
            f"EventSubscription(callback={self.callback}, "
            f"event_type={self.event_type}, id={self.id})"
        )

    __str__ = __repr__
