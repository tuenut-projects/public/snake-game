import abc
import uuid


class ABCGameObject(abc.ABC):
    x_pos: int
    y_pos: int

    def __init__(self, x_pos: int = None, y_pos: int = None):
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.uuid = uuid.uuid4()

    @property
    def position(self) -> tuple[int, int]:
        return self.x_pos, self.y_pos

    @abc.abstractmethod
    def get_dto(self): ...
