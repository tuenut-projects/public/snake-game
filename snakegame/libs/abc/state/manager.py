import abc


class ABCGameStateManager(abc.ABC):
    @abc.abstractmethod
    def __init__(self): ...

    @abc.abstractmethod
    def update(self): ...
