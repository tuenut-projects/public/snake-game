from typing import Literal
from uuid import UUID

from pydantic import BaseModel


class BaseDTO(BaseModel):
    uuid: UUID
    type: Literal["snake_head", "apple", "snake_body"]
