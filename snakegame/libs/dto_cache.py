from uuid import UUID

from snakegame.libs.abc.state.dto import BaseDTO


class DTOCache:
    cache: dict[UUID, BaseDTO]
    updated: list[BaseDTO]

    def __init__(self):
        self.cache = {}
